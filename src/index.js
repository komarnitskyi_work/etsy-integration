const express = require('express');
const cookieParser = require('cookie-parser');
const fs = require('fs');
const url = require('url');
const client = require('./client');
const boolean = require('./helpers').boolean;

app = express();

app.use(cookieParser('secEtsy'));

app.get('/login', function (req, res) {
	client.getOAuthRequestToken(function (error, oauth_token, oauth_token_secret, response) {
		const loginUrl = response.login_url;
		client.access.token = oauth_token;
		client.access.secret = oauth_token_secret;
		return res.redirect(loginUrl);
	});
});

app.get('/auth', function (req, res) {
	const query = url.parse(req.url, true).query;
	const verifier = query.oauth_verifier;

	return client.getOAuthAccessToken(client.access.token, client.access.secret, verifier, function (err, oauth_access_token, oauth_access_token_secret) {
		client.access.token = oauth_access_token;
		client.access.secret = oauth_access_token_secret;
		fs.writeFileSync('./access.json', JSON.stringify(client.access));

		return res.redirect('/find');
	});
});

app.use(function (req, res, next) {
	client.access.token && client.access.secret
		? next()
		: res.redirect('/login');
})

app.get('/receipt/:id', async function (req, res) {
	const id = parseFloat(req.params.id);
	if (Number.isNaN(id)) return req.send('Bad request', 400);
	return await client.getFullOrder(id);

});

// update status
app.post('/receipt/:id', async function (req, res) {
	const id = parseFloat(req.params.id);
	if (Number.isNaN(id)) return req.send('Bad request', 400);

	const query = url.parse(req.url, true).query;
	const type = query.type;
	if (!["paid", "shipped"].includes(type)) return req.send('Bad request', 400);
	const value = boolean(query.value);

	try {
		await client.putAsync(`https://openapi.etsy.com/v2/receipts/${id}?was_${type}=${value}`, access.token, access.secret);
		return res.send('success');
	} catch (e) {
		console.warn(e);
		return res.send('error');
	}
});

const server = app.listen(3000, function () {
	return console.log('Listening on port %d', server.address().port);
});
