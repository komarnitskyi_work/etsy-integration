function boolean(value) {
	switch (value) {
		case 'true':
		case 'on':
		case 'yes':
		case '1':
		case 1:
		case true:
			return true;
		case 'false':
		case 'off':
		case 'no':
		case '0':
		case 0:
		case false:
			return false;
		default:
			return Boolean(value);
	}
}

module.exports = {
	boolean,
}
