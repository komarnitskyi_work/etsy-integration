module.exports = {
	apps: [
	  {
		name: 'API',
		script: 'index.js',
		env: {},
		instances: 1,
		exec_mode: 'cluster',
		watch: true,
		autorestart: true
	  },
	  {
		name: 'ETSY CRON',
		script: "jobs/com.etsy.js",
		instances: 1,
		exec_mode: 'fork',
		cron_restart: "*/30 * * * *",
		watch: false,
		autorestart: false
	  }
	]
  };
