const client = require('./../client');
const fs = require('fs');

const main = async function () {
	try {
		const body = await client.getAsync(`https://openapi.etsy.com/v2/shops/${client.CMS_ID}/receipts`);

		const receipts = JSON.parse(body).results;
		const orders = await Promise.all(receipts.map(r => client.getFullOrder(r.receipt_id)));
		fs.writeFileSync(`./${+(new Date())}-order.json`, JSON.stringify(orders));

	} catch (e) {
		console.log(e);
		return e;
	}
}


main();
