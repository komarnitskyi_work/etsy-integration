const fs = require('fs');
const OAuth = require('oauth');

// Yeah, it's not okay to keep api keys inside the code
const API_KEY = process.env.ETSY_API_KEY || '2t4p8r111go9jcwac0oyjchf';
const CMS_ID = process.env.ETSY_CMS_ID || 'SilpoKappa';
const SECRET = process.env.ETSY_SECRET || '7xt0fxh7r8';

const access = fs.existsSync('./access.json') ? JSON.parse(fs.readFileSync('./access.json')) : {};

const client = new OAuth.OAuth(
	'https://openapi.etsy.com/v2/oauth/request_token?scope=email_r%20profile_r%20profile_w%20address_r%20address_w%20transactions_r%20transactions_w%20listings_r%20address_r',
	'https://openapi.etsy.com/v2/oauth/access_token',
	API_KEY,
	SECRET,
	'1.0',
	"http://localhost:3000/auth",
	'HMAC-SHA1'
);

client.CMS_ID = CMS_ID;
client.access = access;

client.getAsync = function (url) {
	return new Promise((resolve, reject) => this.get(url, access.token, access.secret, function (error, response) {
		if (error) return reject(error);
		resolve(response);
	}));
}

client.postAsync = function (url, oauth_token, oauth_token_secret, post_body, post_content_type) {
	return new Promise((resolve, reject) => this.post(url, oauth_token, oauth_token_secret, post_body, post_content_type, function (error, response) {
		if (error) return reject(error);
		resolve(response);
	}));
}

client.putAsync = function (url, oauth_token, oauth_token_secret, post_body, post_content_type) {
	return new Promise((resolve, reject) => this.put(url, oauth_token, oauth_token_secret, post_body, post_content_type, function (error, response) {
		if (error) return reject(error);
		resolve(response);
	}));
}

client.getFullOrder = async function (id) {
	try {
		const data = JSON.parse(
			await client.getAsync(`https://openapi.etsy.com/v2/receipts/${id}`, access.token, access.secret)
		).results.pop();
		const response = {};
		response.OUTVIO_PARAMS = {
			API_KEY,
			CMS_ID,
			id: data.receipt_id,
			dateTime: (new Date(data.creation_tsz)).toISOString()
		};

		response.payment = {
			status: data.was_paid ? 'paid' : 'not paid',
			method: data.payment_method,
			total: parseFloat(data.total_price),
			currency: data.currency_code,
			tax: parseFloat(data.total_tax_cost)
		};

		const countryCode = JSON.parse(
			await client.getAsync(`https://openapi.etsy.com/v2/countries/${data.country_id}`, access.token, access.secret)
		).results.pop().iso_country_code;


		const invoicing = {};

		try {
			const buyerInfo = JSON.parse(
				await client.getAsync(`https://openapi.etsy.com/v2/users/${data.buyer_user_id}/addresses`, access.token, access.secret)
			).results.pop();

			invoicing.name = buyerInfo.name;
			invoicing.postcode = buyerInfo.zip;
			invoicing.countryCode = JSON.parse(
				await client.getAsync(`https://openapi.etsy.com/v2/countries/${buyerInfo.country_id}`, access.token, access.secret)
			).results.pop().iso_country_code;
			invoicing.state = buyerInfo.state;
			invoicing.city = buyerInfo.city;
			invoicing.address = `${buyerInfo.first_line} ${buyerInfo.second_line}`.trim();
			invoicing.email = JSON.parse(
				await client.getAsync(`https://openapi.etsy.com/v2/users/${data.buyer_user_id}/addresses`, access.token, access.secret)
			).results.pop().primary_email;
		} catch (e) {
			console.warn(e);
		}

		const products = JSON.parse(
			await client.getAsync(`https://openapi.etsy.com/v2/receipts/${id}/listings`, access.token, access.secret)
		).results;

		response.products = products.map(product => ({
			name: product.title,
			price: parseFloat(product.price),
			quantity: parseFloat(product.quantity),
			sku: product.sku.join(' '),
			description: product.description,
			weight: product.item_weight ? parseFloat(product.item_weight) : undefined
		}));

		response.client = {
			delivery: {
				name: data.name,
				postcode: data.zip,
				countryCode,
				state: data.state,
				city: data.city,
				address: `${data.first_line} ${data.second_line}`.trim(),
				email: data.buyer_email,
				comment: data.message_from_payment

			},
			invoicing
		};

		response.shipping = {
			price: parseFloat(data.total_shipping_cost),
			method: data.shipping_details.shipping_method
		};

		return response

	} catch (err) {
		console.warn(err);
		return error;
	}
}

module.exports = client;

